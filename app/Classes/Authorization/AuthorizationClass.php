<?php

namespace App\Classes\Authorization;

use Dlnsk\HierarchicalRBAC\Authorization;


/**
 *  This is description of hierarchical RBAC (Role Based Access Control) authorization configuration.
 */

class AuthorizationClass extends Authorization
{
    /**
     * All permissions -- need to update this list when making edits
     * 'permission name' => [
     *      'description' is a description of role
     *      'next' is a child role
     *      'equal' is a role 
     * ]
     */
	public function getPermissions() {
		return [
		    ### Articles
            # Edit
			'update-article' => [
					'description' => 'Edit all articles',
					'next' => 'edit-own-article',
				],
			'update-own-article' => [
					'description' => 'Edit own article',
				],

            # Create
            'create-article' => [
                'description' => 'Create articles'
            ],

            # Delete
            'delete-article' => [
                'description' => 'Delete any articles',
            ],

            'delete-own-article' => [
                'description' => 'Delete own article',
                'equal' => 'update-post'
            ],

            # Read
            'read-article-list' => [
                'description' => 'Read list of all articles'
            ],

            ### Categories
            # Create
            'create-category' => [
                'description' => 'Create categories',
                'next' => 'editCategory'
            ],

            # Update
            'update-category' => [
                'description' => 'Edit categories'
            ],

            # Delete
            'delete-category' => [
                'description' => 'Delete categories',
                'equal' => 'update-category'
            ],

            # Read
            'read-category-list' => [
                'description' => 'Read list of all categories'
            ],

            ### Users
            # Create
            'create-user' => [
                'description' => 'Create categories',
                'next' => 'editCategory'
            ],

            # Edit
            'update-user' => [
                'description' => 'Edit categories'
            ],

            # Delete
            'delete-user' => [
                'description' => 'Delete categories',
                'equal' => 'update-user'
            ],

            # Read
            'read-user-list' => [
                'description' => 'Read list of all categories'
            ],
		];
	}

	public function getRoles() {
		return [
			'user' => [
                'create-article',
                'update-own-article',
                'read-article-list'
            ],
		];
	}
}
