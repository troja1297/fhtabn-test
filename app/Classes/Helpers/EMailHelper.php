<?php


namespace App\Classes\Helpers;


class EMailHelper
{
    public function sentEmail(string $summary, string $body, string $email) {
        info("{$summary} of {$email} is {$body}");
    }
}
