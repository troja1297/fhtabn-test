<?php


namespace App\Classes\Helpers;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class FileHelper
{


    /**
     * Save file to storage
     *
     * @param $image
     * @param string $path path to storage
     * @return string
     */
    public function saveFile($image, string $path) {
        $fileName = time() . '.' . $image->getClientOriginalExtension();

        $img = Image::make($image->getRealPath());

        $img->stream();


        Storage::disk('public')->put($path.$fileName, $img, 'public');
        return $path.$fileName;
    }

    public function deleteFile($path) {
        Storage::disk('public')->delete($path);
    }

    /**
     *
     * Return unique file name for uploading file;
     *
     * @return string
     */
    public function hashName()
    {
        return Str::random(40);
    }
}
