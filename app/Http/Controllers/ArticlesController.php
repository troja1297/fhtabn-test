<?php

namespace App\Http\Controllers;

use App\Http\Models\Article;
use App\Http\Models\Category;
use App\Http\Requests\ArticleRequest;
use App\Classes\Helpers\FileHelper;
use App\User;
use Illuminate\Http\FileHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ArticlesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * GET
     * Argument is the id of a current article.
     * Returns the update update article view.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateArticle($id) {
        return view('content.updateArticle', [
            'categories' => Category::whereNull('parent_id')->with('childCategories')->get(),
            'article' => Article::find($id)]);
    }

    /**
     * GET
     * Return a create article view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createArticle() {
        return view('content.createArticle', ['categories' =>
            Category::whereNull('parent_id')->with('childCategories')->get()]);
    }

    /**
     * GET
     * Method returns a list of article with view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all() {
        return view('/content/articlesList', ['data' => DB::table('articles')
            ->join('categories', 'articles.category_id', '=', 'categories.id')
            ->select('articles.*', 'categories.title as categoryTitle')->get()]);
    }

    /**
     * GET
     * Argument is the id of a current article.
     * Method returns a current article with view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        $article = Article::find($id);
        return view('content.article', ['article' => $article, 'category' => Category::where('id', '=', $article->category_id)->first()]);
    }

    /**
     * POST
     * Argument is a ArticleRequest validation result.
     * Method result is redirect to the articles view
     *
     * @param ArticleRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(ArticleRequest $req) {
        $article = new Article();
        $fileHelper = new FileHelper();
        $article->title = $req->input('title');
        $article->description = $req->input('description');
        $article->user_id = auth()->user()->id;
        $article->category_id = $req->input('category');
        $article->image = $fileHelper->saveFile($req->image, 'images/articles/');

        $article->save();

        return redirect()->route('all-articles')->with('success', 'Article was created');
    }

    /**
     * Argument of function is id of updating article and request with result of validation fields;
     * On result redirect to all categories view.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, ArticleRequest $req) {
        $article = Article::find($id);
        $fileHelper = new FileHelper();
        $article->title = $req->input('title');
        $article->description = $req->input('description');
        $article->user_id = auth()->user()->id;
        $article->category_id = $req->input('category');
        if ($req->hasFile('image') == true) {
            $fileHelper->deleteFile($article->image);
            $article->image = $fileHelper->saveFile($req->image, 'images/articles/'.$req->input('image'));
        }

        $article->save();

        return redirect()->route('all-articles')->with('success', 'Article was created');
    }

    /**
     * POST
     * Argument of function is id of deleting article;
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $article = Article::find($id);
        if ($article->user_id == auth()->user()->id || auth()->user()->admin == 1) {
            (new FileHelper())->deleteFile($article);
            $article->delete();
            return redirect()->route('all-articles')->with('success', 'Article was deleted!');
        }
        return response('Unauthorized.', 401);
    }
}
