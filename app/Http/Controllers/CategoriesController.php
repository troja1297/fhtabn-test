<?php

namespace App\Http\Controllers;

use App\Http\Models\Article;
use App\Http\Models\Category;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * GET
     * Argument is the id of current category.
     * Returns the update category view.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateCategory($id) {
        return view('content.updateCategory', [
            'currentCategory' => Category::find($id),
            'categories' => Category::whereNull('parent_id')->with('childCategories')->get()
        ]);
    }

    /**
     * GET
     * Return a create category view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createCategory() {
        return view('content.createCategory', ['categories' =>
            Category::whereNull('parent_id')->with('childCategories')->get()]);
    }

    /**
     * GET
     * Method returns a list of categories with view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all() {
        return view('content.categoriesList', ['categories' =>
            Category::whereNull('parent_id')->with('childCategories')->get()]);
    }

    /**
     * POST
     * Argument is a CategoryRequest validation result.
     * Method result is redirect to the categories view
     *
     * @param CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(CategoryRequest $request) {
        $category = new Category();
        $category->title = $request->input('title');
        if (Category::where('id', '=', $request->input('parent_category'))->exists()) {
            $category->parent_id = $request->input('parent_category');
            $category->isChild = true;
        }
        $category->save();

        return redirect()->route('all-categories')->with('success', 'Category was created');
    }

    /**
     * POST
     * Argument of function is id of deleting category;
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        Category::find($id)->delete();
        return redirect()->route('all-categories')->with('success', 'Category was deleted!');
    }

    /**
     * POST
     * Argument of function is id of updating category and request with result of validation fields;
     * On result redirect to all categories view.
     * @param $id
     * @param CategoryRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, CategoryRequest $req) {
        $category = Category::find($id);
        $category->title = $req->input('title');
        if (Category::where('id', '=', $req->input('parent_category'))->exists()) {
            $category->parent_id = $req->input('parent_category');
            $category->isChild = true;
        }
        $category->save();
        return redirect()->route('all-categories')->with('success', 'Category was updated!');
    }
}
