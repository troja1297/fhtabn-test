<?php

namespace App\Http\Controllers;

use App\Classes\Helpers\EMailHelper;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * GET
     * Argument is the id of a current user.
     * Returns the update update user view.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateUser($id) {
        return view('content.updateUser', ['user' => User::find($id)]);
    }

    /**
     * GET
     * Return a create user view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createUser() {
        return view('content.createUser');
    }

    /**
     * GET
     * Method returns a list of users with view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all() {
        return view('content.usersList', ['data' => User::all()->where('id', '!=', auth()->user()->id)]);
    }

    /**
     * POST
     * Argument is a UserRequest validation result.
     * Method result is redirect to the users view
     *
     * @param UserRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(UserRequest $req) {
        $user = new User();
        $userPassword = Str::random(8);
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        $user->isFirstAuthorization = true;
        $user->password = Hash::make($userPassword);
        $user->admin = $req->input('isAdmin') == null ? 0 : 1;

        $user->save();
        (new EMailHelper())->sentEmail('Password', $userPassword, $user->email);

        return redirect()->route('home')->with('success', 'User was created!');
    }

    /**
     * POST
     * Argument of function is id of deleting user;
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        User::find($id)->delete();
        return redirect()->route('all-users')->with('success', 'User was deleted!');
    }

    /**
     * POST
     * Argument of function is id of updated user and request with result of validation fields;
     * On result redirect to all users view.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UserRequest $req) {
        $user = User::find($id);
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        $user->isFirstAuthorization = $req->input('needToChangePass') == null ? 0 : 1;
        $user->admin = $req->input('isAdmin') == null ? 0 : 1;

        $user->save();
        return redirect()->route('all-users')->with('success', 'User was updated!');
    }
}
