<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'parent_category',
    ];

    public function categories() {
        return $this->hasMany(Category::class, 'parent_id')->with('categories');
    }

    public function childCategories()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('categories');
    }

    public function articles() {
        return $this->hasMany(Category::class);
    }
}
