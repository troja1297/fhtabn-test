<?php

namespace App\Http\Requests;

use App\Http\Models\Category;
use App\User;
use App\ValidationRules\CategoryIsChildRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getCustomRules($this->input('validation_type'));
    }

    public function getCustomrules($validationType) {
        $rules = [];

        switch ($validationType) {
            case 'create':
                $rules = [
                    'title' => 'unique:categories,title|required|min:3|max:20',
                    'parent_category' => [new CategoryIsChildRule()]
                ];
                break;
            case 'update':
                $category = Category::find($this->id);
                $rules = [
                    'title' => ['required', 'min:3', 'max:20', Rule::unique('categories', 'title')->ignore($category->id)],
                    'parent_category' => [new CategoryIsChildRule()]
                ];
                break;
        }

        return $rules;
    }
}
