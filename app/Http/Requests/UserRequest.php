<?php


namespace App\Http\Requests;


use App\User;
use App\ValidationRules\CategoryIsChildRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getCustomRules($this->input('validation_type'));
    }

    public function getCustomrules($validationType) {
        $rules = [];

        switch ($validationType) {
            case 'create':
                $rules = [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users']
                ];
                break;
            case 'update':
                $user = User::find($this->id);
                $rules = [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->ignore($user->id)]
                ];
                break;
        }

        return $rules;
    }
}
