<?php


namespace App\ValidationRules;


use App\Http\Models\Category;
use Illuminate\Contracts\Validation\Rule;

class CategoryIsChildRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $category = Category::find($value);
        if ($category->isChild == true) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This category is already a subsidiary.';
    }
}
