<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'isFirstAuthorization' => true,
            'name' => 'user1',
            'email' => 'user1@gmail.com',
            'admin' => 0,
            'password' => Hash::make('Admin1'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'isFirstAuthorization' => true,
            'name' => 'user2',
            'email' => 'user2@gmail.com',
            'admin' => 0,
            'password' => Hash::make('Admin1'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'isFirstAuthorization' => true,
            'name' => 'user3',
            'email' => 'user3@gmail.com',
            'admin' => 0,
            'password' => Hash::make('Admin1'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'isFirstAuthorization' => false,
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'admin' => 1,
            'password' => Hash::make('root'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('categories')->insert([
            'id' => 1,
            'title' => 'Животные',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('categories')->insert([
            'id' => 2,
            'title' => 'Млекопитающие',
            'parent_id' => 1,
            'isChild' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('categories')->insert([
            'id' => 3,
            'title' => 'Рыбы',
            'parent_id' => 1,
            'isChild' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('categories')->insert([
            'id' => 4,
            'title' => 'Птицы',
            'parent_id' => 1,
            'isChild' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('articles')->insert([
            'title' => 'Статья о рыбах',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Purus semper eget duis at tellus at urna condimentum. Varius sit amet mattis vulputate enim. Tincidunt lobortis feugiat vivamus at. Tincidunt dui ut ornare lectus. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Enim diam vulputate ut pharetra sit amet aliquam id. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Sit amet nulla facilisi morbi tempus. Sem integer vitae justo eget. Ut venenatis tellus in metus vulputate eu scelerisque felis. Malesuada nunc vel risus commodo. Sagittis nisl rhoncus mattis rhoncus. Tempus iaculis urna id volutpat lacus laoreet. In nibh mauris cursus mattis molestie. Massa tincidunt nunc pulvinar sapien et ligula. Sed adipiscing diam donec adipiscing tristique risus nec feugiat. Felis eget nunc lobortis mattis aliquam faucibus purus. Pellentesque pulvinar pellentesque habitant morbi tristique senectus.',
            'user_id' => '1',
            'category_id' => '3',
            'image' => 'images/articles/fish.jpeg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('articles')->insert([
            'title' => 'Статья о пингвинах',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Purus semper eget duis at tellus at urna condimentum. Varius sit amet mattis vulputate enim. Tincidunt lobortis feugiat vivamus at. Tincidunt dui ut ornare lectus. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Enim diam vulputate ut pharetra sit amet aliquam id. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Sit amet nulla facilisi morbi tempus. Sem integer vitae justo eget. Ut venenatis tellus in metus vulputate eu scelerisque felis. Malesuada nunc vel risus commodo. Sagittis nisl rhoncus mattis rhoncus. Tempus iaculis urna id volutpat lacus laoreet. In nibh mauris cursus mattis molestie. Massa tincidunt nunc pulvinar sapien et ligula. Sed adipiscing diam donec adipiscing tristique risus nec feugiat. Felis eget nunc lobortis mattis aliquam faucibus purus. Pellentesque pulvinar pellentesque habitant morbi tristique senectus.',
            'user_id' => '1',
            'category_id' => '4',
            'image' => 'images/articles/pinguin.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('articles')->insert([
            'title' => 'Статья о слонах',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Purus semper eget duis at tellus at urna condimentum. Varius sit amet mattis vulputate enim. Tincidunt lobortis feugiat vivamus at. Tincidunt dui ut ornare lectus. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Enim diam vulputate ut pharetra sit amet aliquam id. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Sit amet nulla facilisi morbi tempus. Sem integer vitae justo eget. Ut venenatis tellus in metus vulputate eu scelerisque felis. Malesuada nunc vel risus commodo. Sagittis nisl rhoncus mattis rhoncus. Tempus iaculis urna id volutpat lacus laoreet. In nibh mauris cursus mattis molestie. Massa tincidunt nunc pulvinar sapien et ligula. Sed adipiscing diam donec adipiscing tristique risus nec feugiat. Felis eget nunc lobortis mattis aliquam faucibus purus. Pellentesque pulvinar pellentesque habitant morbi tristique senectus.',
            'user_id' => '1',
            'category_id' => '2',
            'image' => 'images/articles/elephant.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('articles')->insert([
            'title' => 'Статья о зебрах',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Purus semper eget duis at tellus at urna condimentum. Varius sit amet mattis vulputate enim. Tincidunt lobortis feugiat vivamus at. Tincidunt dui ut ornare lectus. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Enim diam vulputate ut pharetra sit amet aliquam id. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Sit amet nulla facilisi morbi tempus. Sem integer vitae justo eget. Ut venenatis tellus in metus vulputate eu scelerisque felis. Malesuada nunc vel risus commodo. Sagittis nisl rhoncus mattis rhoncus. Tempus iaculis urna id volutpat lacus laoreet. In nibh mauris cursus mattis molestie. Massa tincidunt nunc pulvinar sapien et ligula. Sed adipiscing diam donec adipiscing tristique risus nec feugiat. Felis eget nunc lobortis mattis aliquam faucibus purus. Pellentesque pulvinar pellentesque habitant morbi tristique senectus.',
            'user_id' => '1',
            'category_id' => '2',
            'image' => 'images/articles/hippo.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('articles')->insert([
            'title' => 'Статья о львах',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Purus semper eget duis at tellus at urna condimentum. Varius sit amet mattis vulputate enim. Tincidunt lobortis feugiat vivamus at. Tincidunt dui ut ornare lectus. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Enim diam vulputate ut pharetra sit amet aliquam id. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Sit amet nulla facilisi morbi tempus. Sem integer vitae justo eget. Ut venenatis tellus in metus vulputate eu scelerisque felis. Malesuada nunc vel risus commodo. Sagittis nisl rhoncus mattis rhoncus. Tempus iaculis urna id volutpat lacus laoreet. In nibh mauris cursus mattis molestie. Massa tincidunt nunc pulvinar sapien et ligula. Sed adipiscing diam donec adipiscing tristique risus nec feugiat. Felis eget nunc lobortis mattis aliquam faucibus purus. Pellentesque pulvinar pellentesque habitant morbi tristique senectus.',
            'user_id' => '1',
            'category_id' => '2',
            'image' => 'images/articles/lion.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('articles')->insert([
            'title' => 'Статья о птицах',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Purus semper eget duis at tellus at urna condimentum. Varius sit amet mattis vulputate enim. Tincidunt lobortis feugiat vivamus at. Tincidunt dui ut ornare lectus. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Enim diam vulputate ut pharetra sit amet aliquam id. Facilisi cras fermentum odio eu feugiat pretium nibh ipsum. Sit amet nulla facilisi morbi tempus. Sem integer vitae justo eget. Ut venenatis tellus in metus vulputate eu scelerisque felis. Malesuada nunc vel risus commodo. Sagittis nisl rhoncus mattis rhoncus. Tempus iaculis urna id volutpat lacus laoreet. In nibh mauris cursus mattis molestie. Massa tincidunt nunc pulvinar sapien et ligula. Sed adipiscing diam donec adipiscing tristique risus nec feugiat. Felis eget nunc lobortis mattis aliquam faucibus purus. Pellentesque pulvinar pellentesque habitant morbi tristique senectus.',
            'user_id' => '1',
            'category_id' => '4',
            'image' => 'images/articles/bird.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
