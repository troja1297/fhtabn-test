@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <h3 class="panel-heading">Change password</h3>
                    <div class="panel-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <form class="form-horizontal" method="POST" action="{{ route('changePassword') }}">
                            @csrf
                            <div class="form-group">
                                <label for="current-password" class="col-md-4 control-label">Current Password</label>
                                <div class="col-md-6">
                                    <input id="current-password"  type="password"  class="form-control @if(count($errors->get('current-password')) != 0) is-invalid @endif" name="current-password">
                                    @foreach ($errors->get('current-password') as $message)
                                        <div class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new-password" class="col-md-4 control-label">New Password</label>
                                <div class="col-md-6">
                                    <input id="new-password" type="password"  class="form-control @if(count($errors->get('new-password')) != 0) is-invalid @endif" name="new-password">
                                    @foreach ($errors->get('new-password') as $message)
                                        <div class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new-password-confirm" class="col-md-4 control-label">Confirm New Password</label>
                                <div class="col-md-6">
                                    <input id="new-password-confirm" type="password"  class="form-control @if(count($errors->get('new-password-confirm')) != 0) is-invalid @endif" name="new-password_confirmation">
                                </div>
                                @foreach ($errors->get('new-password-confirm') as $message)
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Change Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
