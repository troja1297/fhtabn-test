@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div style="display: flex; justify-content: space-between;">
                    <h4>{{$category->title}}</h4>
                    <div>
                        @if(auth()->user()->id == $article->user_id || auth()->user()->admin == 1)
                            <a href="{{route('update-article', $article->id)}}" class="btn btn-secondary">Edit</a>
                            <a href="{{route('delete-article', $article->id)}}" class="btn btn-danger">Delete</a>
                        @endif
                    </div>
                </div>
                <h5 class="mb-1">{{$article->title}}</h5>
                <img src="{{asset(Storage::url('public/'.$article->image))}}" style="margin-top: 10px;" class="img-fluid" alt="">
                <div class="d-flex w-100 justify-content-between">
                    <small style="padding: 10px;">{{$article->created_at}}</small>
                </div>
                <p class="mb-1">{{$article->description}}</p>
            </div>
        </div>
    </div>
@endsection
