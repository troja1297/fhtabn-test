@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a class="btn btn-secondary btn-lg btn-block" href="{{ route('create-article') }}">{{ __('New Article') }}</a>
                @foreach($data as $item)
                    <div class="list-group">
                        <a href="{{route('show-one-article', $item->id)}}" style="margin-bottom: 10px;" class="list-group-item list-group-item-action flex-column align-items-start">
                            <h4>{{$item->categoryTitle}}</h4>
                            <h5 class="mb-1">{{$item->title}}</h5>
{{--                            <img src="{{asset(Storage::url($item->image))}}" style="margin-top: 10px;" class="img-fluid" alt="">--}}
                            <div class="d-flex w-100 justify-content-between">
                                <small style="padding: 10px;">{{$item->created_at}}</small>
                            </div>
                            <p class="mb-1">{{$item->description}}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
