@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a class="btn btn-secondary btn-lg btn-block" href="{{ route('create-category') }}">{{ __('New Category') }}</a>
                <ul class="list-group">
                    @if(count($categories) > 0)
                        @foreach ($categories as $category)
                            <li style="display: flex; justify-content: space-between;" class="list-group-item">
                                <div>
                                    {{$category->title}}
                                </div>
                                <div>
                                    <a href="{{route('update-category', $category->id)}}" class="btn btn-secondary">Edit</a>
                                    <a href="{{route('delete-category', $category->id)}}" class="btn btn-danger">Delete</a>
                                </div>
                            </li>
                            @if(count($category->childCategories))
                                @foreach ($category->childCategories as $subCategories)
                                    <li class="list-group-item">
                                        <div style="display: flex; justify-content: space-between;">
                                            <div style="display: flex;">
                                                <div style="width: 30px">
                                                    <svg class="bi bi-chevron-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L10.293 8 4.646 2.354a.5.5 0 010-.708z" clip-rule="evenodd"/>
                                                    </svg>
                                                </div>
                                                <div>
                                                    {{$subCategories->title}}
                                                </div>
                                            </div>
                                            <div>
                                                <a href="{{route('update-category', $subCategories->id)}}" class="btn btn-secondary">Edit</a>
                                                <a href="{{route('delete-category', $subCategories->id)}}" class="btn btn-danger">Delete</a>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
