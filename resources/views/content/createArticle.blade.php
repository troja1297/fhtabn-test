@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h3>Create category</h3>
                <form method="POST" action="{{route('create-article-form')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="category">Set a category:</label>
                        <select class="form-control @if(count($errors->get('category')) != 0) is-invalid @endif" id="category" name="category" autocomplete="category" focus>
                            <option value="" disabled selected>Please select parent category</option>
                            @if(count($categories) > 0)
                                @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                    @if(count($category->childCategories))
                                        @foreach ($category->childCategories as $subCategories)
                                            <option style="text-indent: 10px" value="{{$subCategories->id}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$subCategories->title}}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </select>
                        @foreach ($errors->get('category') as $message)
                            <div class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group">
                        <label for="image">Set image:</label>
                        <input type="file" class="form-control-file @if(count($errors->get('image')) != 0) is-invalid @endif" id="image" autocomplete="image" name="image">
                        @foreach ($errors->get('image') as $message)
                            <div class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group">
                        <label for="title">Article Title:</label>
                        <input type="text" name="title" id="title" class="form-control @if(count($errors->get('title')) != 0) is-invalid @endif" placeholder="Write a category name" autocomplete="title">
                        @foreach ($errors->get('title') as $message)
                            <div class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" id="description" class="form-control @if(count($errors->get('description')) != 0) is-invalid @endif" placeholder="Description" autocomplete="description"></textarea>
                        @foreach ($errors->get('description') as $message)
                            <div class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection



