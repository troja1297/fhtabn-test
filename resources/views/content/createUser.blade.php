@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h3>Create user</h3>
                <form method="POST" action="{{ route('create-user-form') }}">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @if(count($errors->get('name')) != 0) is-invalid @endif" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control @if(count($errors->get('email')) != 0) is-invalid @endif" name="email" value="{{ old('email') }}" autocomplete="email">

                            @foreach ($errors->get('email') as $message)
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <input name="validation_type" type="hidden" id="validation_type" value="create">
                    <div class="form-group row">
                        <label class="form-check-label" for="isAdmin">
                            Is Admin:
                        </label>
                        <div class="col-md-6">
                            <input class="form-check-input @if(count($errors->get('isAdmin')) != 0) is-invalid @endif" type="checkbox" value="1" name="isAdmin" id="isAdmin">
                            @foreach ($errors->get('isAdmin') as $message)
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                </form>
            </div>
        </div>
    </div>

@endsection
