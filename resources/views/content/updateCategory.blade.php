@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h3>Update category</h3>
                <form method="POST" action="{{route('update-category-form', $currentCategory->id)}}">
                    @csrf
                    <div class="form-group">
                        <label for="parent_category">Set a parent category:</label>
                        <select class="form-control @if(count($errors->get('parent_category')) != 0) is-invalid @endif" id="parent_category" name="parent_category" autocomplete="parent_category" focus>
                            <option value="" disabled selected>Please select parent category</option>
                            @if(count($categories) > 0)
                                @foreach ($categories as $category)
                                    @if($currentCategory->parent_id == $category->id)
                                        <option value="{{$category->id}}" selected>{{$category->title}}</option>
                                    @else
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endif
                                    @if(count($category->childCategories))
                                        @foreach ($category->childCategories as $subCategories)
                                                <option style="text-indent: 10px" value="{{$subCategories->id}}">&nbsp;&nbsp;&nbsp;&nbsp;{{$subCategories->title}}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </select>
                        @foreach ($errors->get('parent_category') as $message)
                            <div class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endforeach
                    </div>
                    <input name="validation_type" type="hidden" id="validation_type" value="update">
                    <div class="form-group">
                        <label for="title">Category Title:</label>
                        <input type="text" name="title" id="title" value="{{$currentCategory->title}}" class="form-control @if(count($errors->get('title')) != 0) is-invalid @endif" placeholder="Write a category name" autocomplete="title">
                        @foreach ($errors->get('title') as $message)
                            <div class="invalid-feedback">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>

@endsection
