@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a class="btn btn-secondary btn-lg btn-block" href="{{ route('create-user') }}">{{ __('New User') }}</a>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">name</th>
                        <th scope="col">email</th>
                        <th scope="col">action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $number = 0;
                    @endphp
                    @foreach($data as $item)
                        <tr>
                            <th scope="row">{{$number+=1}}</th>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>
                                <a href="{{route('update-user', $item->id)}}" class="btn btn-secondary">Edit</a>
                                <a href="{{route('delete-user', $item->id)}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
