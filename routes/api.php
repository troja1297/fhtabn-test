<?php

use App\Http\Models\Article;
use App\Http\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
Route::middleware('auth:web')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('articles', function() {
    return response()->json(['success' => 'Success', 'article'=>Article::all()]);
});

Route::get('categories', function() {
    return response()->json(['success' => 'Success', 'categories'=>Category::all()]);
});
