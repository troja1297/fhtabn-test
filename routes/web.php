<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Change password request group
Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
Route::get('/home', 'HomeController@index')->name('home');

// Article request group
Route::get('/createArticle', 'ArticlesController@createArticle')->name('create-article');
Route::get('/articles', 'ArticlesController@all')->name('all-articles');
Route::get('/articles/{id}', 'ArticlesController@show')->name('show-one-article');
Route::get('/articles/update/{id}', 'ArticlesController@updateArticle')->name('update-article');
Route::post('/articles/update/{id}', 'ArticlesController@update')->name('update-article-form');
Route::get('/articles/delete/{id}', 'ArticlesController@delete')->name('delete-article');
Route::post('/createArticle/submit', 'ArticlesController@create')->name('create-article-form');



Route::group(['middleware' => 'admin'], function () {
    // Category request group
    Route::get('/createCategory', 'CategoriesController@createCategory')->name('create-category');
    Route::get('/categories', 'CategoriesController@all')->name('all-categories');
    Route::get('/categories/update/{id}', 'CategoriesController@updateCategory')->name('update-category');
    Route::post('/categories/update/{id}', 'CategoriesController@update')->name('update-category-form');
    Route::get('/categories/delete/{id}', 'CategoriesController@delete')->name('delete-category');
    Route::post('/createCategory/submit', 'CategoriesController@create')->name('create-category-form');

    // Users request group
    Route::get('/createUser', 'UserController@createUser')->name('create-user');
    Route::get('/users', 'UserController@all')->name('all-users');
    Route::get('/users/update/{id}', 'UserController@updateUser')->name('update-user');
    Route::post('/users/update/{id}', 'UserController@update')->name('update-user-form');
    Route::get('/users/delete/{id}', 'UserController@delete')->name('delete-user');
    Route::post('/createUser/create', 'UserController@create')->name('create-user-form');
});
